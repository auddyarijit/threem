package com.example.remote;

import com.example.model.DemoPojo;
import com.example.model.FormInvoiceResponse;
import com.example.model.InvoiceHistoryResponse;
import com.example.model.LoginResponse;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api {

    @FormUrlEncoded
    @POST("api_upload")
    Call<DemoPojo> uploadImage(@Field("image") ArrayList<String> image);


    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> login(
            @Field("username") String username,@Field("password") String password );


    @FormUrlEncoded
    @POST("create_invoice")
    Call<FormInvoiceResponse> invoiceForm(
            @Field("registration") String registration,
            @Field("chesis") String chesis,
            @Field("registrationYear") String registrationYear,
            @Field("engineNo") String engineNo,
            @Field("vechicleModel") String vechicleModel,
            @Field("vechicleMake") String vechicleMake,

            @Field("personName") String personName,
            @Field("personContact") String personContact,

            @Field("ownerName") String ownerName,
            @Field("gstNo") String gstNo,
            @Field("ownerContact") String ownerContact,
            @Field("address") String address,


            @Field("img_front") String img_front,
            @Field("img_back") String img_back,
            @Field("img_side1") String img_side1,
            @Field("img_side2") String img_side2,


            @Field("bussinessId") String bussinessId,
            @Field("userId") String userId,


            @Field("red20_id") String red20_id,
            @Field("white20_id") String white20_id,

            @Field("red20_qty") String red20_qty,
            @Field("white20_qty") String white20_qty,


            @Field("red50_id") String red50_id,
            @Field("white50_id") String white50_id,
            @Field("yellow50_id") String yellow50_id,

            @Field("red50_qty") String red50_qty,
            @Field("white50_qty") String white50_qty,
            @Field("yellow50_qty") String yellow50_qty,

            @Field("red80_id") String red80_id,
            @Field("white80_id") String white80_id,
            @Field("yellow80_id") String yellow80__id,

            @Field("white80_qty") String white80_qty,
            @Field("red80_qty") String red80_qty,
            @Field("yellow80_qty") String yellow80_qty);




    @GET("invoice_history")
    Call<InvoiceHistoryResponse> invoiceResponse(@Query("userId") String userId);

}
