package com.example.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.threem.R;

import java.util.ArrayList;

public class VechicalDetailsActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private EditText edtRegistration,edtChesis;
    private EditText edtRegistrationYear,edtEngineNo;

  //  private EditText edtVechicleModel,edtVechicleMake;

    private EditText edtPersonName,edtPersonContact;
    private EditText edtAddress,edtOwnerName;
   // private EditText edtDistributorName,edtMFGAddress;
   // private EditText edtDistributorAddress;
    private EditText edtGST;
    private EditText edtOwnerContact;

    String userId =null;
    String bussinessId =null;

    private TextView txtNext,txtActivityName;
    private ImageView imgBack;
    private ImageView imgHome;
    private Spinner spinnerModel,spinnerMaker;

    private  String vehicleModel,vechicleMaker;


    String vechicleNo = "[A-Z]{2}+[0-9]{2}+[A-Z]{2}+[0-9]{4}+";

    String contactNumber =  "^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$";



    String[] spnModelsArray = {"Models","1990", "1991", "1992", "1993","1994", "1995", "1996", "1997","1998",
            "1999","2000", "2001", "2002" ,"2003", "2004", "2005", "2006","2007",
            "2008", "2009" ,"2010", "2011", "2012", "2013","2014","2015", "2016",
            "2017", "2018","2019","2020","2021"};


    ArrayList<String> makerList = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vechical_details);
//        getSupportActionBar().hide();



        init();
        clickEvent();
        getData();

        makerArrayList();

        txtActivityName.setText("Details");

        setSpinnerModelAdapter();
        setSpinnerMakerAdapter();


    }



    private void makerArrayList() {
        makerList.add("Vehicle Maker");
        makerList.add("Ashok Leyland");
        makerList.add("Mahindra & Mahindra");
        makerList.add("Asian Motor Works");
        makerList.add("Tata Motors");
        makerList.add("Atul Auto");
        makerList.add("Volvo India");
        makerList.add("Bajaj Auto");
        makerList.add("Fiat India");
        makerList.add("Force Motors");
        makerList.add("Ford India");
        makerList.add("General Motors India");
        makerList.add("Hindustan Motors");
        makerList.add("Honda");
        makerList.add("Hyundai Motors");
        makerList.add("Volkswagen");
        makerList.add("Skoda");
    }

    private void setSpinnerModelAdapter() {
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,spnModelsArray);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerModel.setAdapter(aa);
    }

    private void setSpinnerMakerAdapter() {
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,makerList);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMaker.setAdapter(aa);
    }

    private void getData() {
        Bundle bundle = getIntent().getExtras();
        if(bundle!=null)
        {
            userId = bundle.getString("userId");
            bussinessId = bundle.getString("bussinessId");
        }
    }

    private void init() {
        txtNext = findViewById(R.id.txtNext);

        edtRegistration = findViewById(R.id.edtRegistration);
        edtChesis = findViewById(R.id.edtChesis);
        edtRegistrationYear = findViewById(R.id.edtRegistrationYear);
        edtEngineNo = findViewById(R.id.edtEngineNo);
        //edtVechicleModel = findViewById(R.id.edtVechicleModel);

       // edtVechicleMake = findViewById(R.id.edtVechicleMake);
        edtPersonName = findViewById(R.id.edtPersonName);
        edtPersonContact = findViewById(R.id.edtPersonContact);
        edtAddress = findViewById(R.id.edtAddress);
        edtOwnerName = findViewById(R.id.edtOwnerName);
        edtGST = findViewById(R.id.edtGST);

        edtOwnerContact = findViewById(R.id.edtOwnerContact);

        spinnerModel = findViewById(R.id.spinnerModel);
        spinnerMaker = findViewById(R.id.spinnerMaker);

        //edtDistributorName = findViewById(R.id.edtDistributorName);
       // edtMFGAddress = findViewById(R.id.edtMFGAddress);
        //edtDistributorAddress = findViewById(R.id.edtDistributorAddress);

        imgBack  = findViewById(R.id.imgBack);
        imgHome  = findViewById(R.id.imgHome);
        imgHome.setVisibility(View.VISIBLE);
        txtActivityName = findViewById(R.id.txtActivityName);
    }

    private void clickEvent() {
        txtNext.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        imgHome.setOnClickListener(this);
        spinnerModel.setOnItemSelectedListener(this);
        spinnerMaker.setOnItemSelectedListener(this);
    }


    private void validation() {

        String registration = edtRegistration.getText().toString();


        String chesis = edtChesis.getText().toString();
        String registrationYear = edtRegistrationYear.getText().toString();
        String engineNo = edtEngineNo.getText().toString();
       // String vechicleModel = edtVechicleModel.getText().toString();
       // String vechicleMake = edtVechicleMake.getText().toString();
        String personName = edtPersonName.getText().toString();

        String personContact = edtPersonContact.getText().toString();
        String ownerContact = edtOwnerContact.getText().toString();


        String address = edtAddress.getText().toString();
        String ownerName = edtOwnerName.getText().toString();
        String gstNo = edtGST.getText().toString();

       // String distributorName = edtDistributorName.getText().toString();
       // String mfgAddress = edtMFGAddress.getText().toString();
        //String distributorAddress = edtDistributorAddress.getText().toString();



        if (registration.length()>10)
        {
            edtRegistration.setError(getString(R.string.full_vechicle_number));
            edtRegistration.requestFocus();
            return;
        }
       else if(registration.isEmpty()){
            edtRegistration.setError(getString(R.string.registration_error));
            edtRegistration.requestFocus();
            return;
        }else if (registration.matches(vechicleNo)){
            edtRegistration.requestFocus();
            //Toast.makeText(getApplicationContext(),"valid vechicle Number",Toast.LENGTH_SHORT).show();
        }
        else  {
            edtRegistration.requestFocus();
           // Toast.makeText(getApplicationContext(),"Invalid vechicle Number", Toast.LENGTH_SHORT).show();
        }




        if(chesis.isEmpty()){
            edtChesis.setError(getString(R.string.chesis_error));
            edtChesis.requestFocus();
            return;
        }


        if(registrationYear.isEmpty()){
            edtRegistrationYear.setError(getString(R.string.registration_year_error));
            edtRegistrationYear.requestFocus();
            return;
        }


        if(engineNo.isEmpty()){
            edtEngineNo.setError(getString(R.string.engineNo_error));
            edtEngineNo.requestFocus();
            return;
        }

       /* if(vechicleModel.isEmpty()){
            edtVechicleModel.setError(getString(R.string.vechicle_model));
            edtVechicleModel.requestFocus();
            return;
        }

        if(vechicleMake.isEmpty()){
            edtVechicleMake.setError(getString(R.string.vechicle_make));
            edtVechicleMake.requestFocus();
            return;
        }
*/

        if(personName.isEmpty()){
            edtPersonName.setError(getString(R.string.owner_details_empty));
            edtPersonName.requestFocus();
            return;
        }



        if(personContact.isEmpty()){
            edtPersonContact.setError(getString(R.string.contact_error));
            edtPersonContact.requestFocus();
            return;
        }

        else if (personContact.equals("")||personContact.length()<10)
        {
            edtPersonContact.setError(getString(R.string.enter_10_digit_valid_contact_number));
            edtPersonContact.requestFocus();
            return;
        }



        if(ownerContact.isEmpty()){
            edtOwnerContact.setError(getString(R.string.contact_error));
            edtOwnerContact.requestFocus();
            return;
        }
        else if (ownerContact.equals("")||ownerContact.length()<10)
        {
            edtOwnerContact.setError(getString(R.string.enter_10_digit_valid_contact_number));
            edtOwnerContact.requestFocus();
            return;
        }



        if(address.isEmpty()){
            edtAddress.setError(getString(R.string.address_empty));
            edtAddress.requestFocus();
            return;
        }

        if(ownerName.isEmpty()){
            edtOwnerName.setError(getString(R.string.manufacture_name_empty));
            edtOwnerName.requestFocus();
            return;
        }

       /* if(distributorName.length()==0){
            edtDistributorName.setError(getString(R.string.distributor_name_empty));
            edtDistributorName.requestFocus();
            return;
        }*/


        /*if(mfgAddress.length()==0){
            edtMFGAddress.setError(getString(R.string.mfg_address_empty));
            edtMFGAddress.requestFocus();
            return;
        }*/


        /*if(distributorAddress.length()==0){
            edtDistributorAddress.setError(getString(R.string.distributor_address_empty));
            edtDistributorAddress.requestFocus();
            return;
        }*/

        else {

            Intent intent = new Intent(VechicalDetailsActivity.this, TapesActivity.class);
            intent.putExtra("registration", registration);
            intent.putExtra("registrationYear", registrationYear);
            intent.putExtra("engineNo", engineNo);
           //intent.putExtra("vechicleModel", vechicleModel);
            //intent.putExtra("vechicleMake", vechicleMake);

            intent.putExtra("personName", personName);
            intent.putExtra("personContact", personContact);


            intent.putExtra("ownerName", ownerName);
            intent.putExtra("address", address);
            intent.putExtra("ownerContact", ownerContact);


            //intent.putExtra("distributorName", distributorName);
           // intent.putExtra("mfgAddress", mfgAddress);
           // intent.putExtra("distributorAddress", distributorAddress);
            intent.putExtra("chesis", chesis);
            intent.putExtra("gstNo", gstNo);

            intent.putExtra("vehicleModel", vehicleModel);
            intent.putExtra("vechicleMaker", vechicleMaker);

            intent.putExtra("bussinessId", bussinessId);
            intent.putExtra("userId", userId);
            startActivity(intent);
        }

    }



    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txtNext:
                 validation();
                 break;

            case R.id.imgBack:
                finish();
                break;

            case R.id.imgHome:
                Intent intent = new Intent(VechicalDetailsActivity.this, DashBoardActivity.class);
                startActivity(intent);
                break;


               default:
                break;
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if(position == 0) {

        } else {
            vehicleModel = String.valueOf(spinnerModel.getItemAtPosition(position));
            vechicleMaker = parent.getItemAtPosition(position).toString();
        }

         return;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}