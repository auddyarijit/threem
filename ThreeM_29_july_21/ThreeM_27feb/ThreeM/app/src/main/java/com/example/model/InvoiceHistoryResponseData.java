package com.example.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InvoiceHistoryResponseData {
    @SerializedName("invoice_id")
    @Expose
    private Integer invoiceId;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("area_name")
    @Expose
    private String areaName;
    @SerializedName("invoice_date")
    @Expose
    private String invoiceDate;

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }
}
