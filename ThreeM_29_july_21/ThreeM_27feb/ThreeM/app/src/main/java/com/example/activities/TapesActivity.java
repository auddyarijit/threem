package com.example.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.threem.R;

import java.util.ArrayList;

public class TapesActivity extends AppCompatActivity implements View.OnClickListener {





    private TextView txtNext,txtActivityName;
    private ImageView imgBack;
    private ImageView imgHome;
    String registration,registrationYear,engineNo,vehicleModel;

   // String vechicleMake;
    String chesis,gstNo,personName,personContact;
    String ownerName,ownerContact,address;

    String vechicleMaker;


   // String contact,distributorName,mfgAddress,distributorAddress,chesis,gstNo;


    private EditText edt20MMRedTap,edt20MMWhiteTap;
    private EditText edt50MMFitmentREDTap,edt50MMWhiteFitment;
    private EditText edt50MMYellowFitment;
    //private EditText edtRearMarkingCase3,edtRearMarkingClass4;
    private EditText edt80MMREDReflector,edt80MMWHITEReflector;
    private EditText edt80MMYELLOWReflector;

    private  TextView txtRedTap20MM_id,txtWhiteTap20MM_id;
    private  TextView txt50MMREDTap_id,txt50MMWhite_id,txt50MMYellow_id;
    private  TextView txtClass4_id,txtClass3_id;
    private  TextView txt80MMREDReflector_id,txt80MMWHITEReflector_id,txt80MMYELLOWReflector_id;

    String userId =null;
    String bussinessId =null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tapes);
       // getSupportActionBar().hide();

        init();
        getData();

        click();

        txtActivityName.setText("Fitment Details");

    }

    private void click() {
        txtNext.setOnClickListener(this);
        imgHome.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }

    private void init() {
        txtNext = findViewById(R.id.txtNext);
        edt20MMRedTap = findViewById(R.id.edt20MMRedTap);
        edt20MMWhiteTap = findViewById(R.id.edt20MMWhiteTap);
        edt50MMFitmentREDTap = findViewById(R.id.edt50MMFitmentREDTap);
        edt50MMWhiteFitment = findViewById(R.id.edt50MMWhiteFitment);
        edt50MMYellowFitment = findViewById(R.id.edt50MMYellowFitment);
       // edtRearMarkingCase3 = findViewById(R.id.edtRearMarkingCase3);
       // edtRearMarkingClass4 = findViewById(R.id.edtRearMarkingClass4);
        edt80MMREDReflector = findViewById(R.id.edt80MMREDReflector);
        edt80MMWHITEReflector = findViewById(R.id.edt80MMWHITEReflector);
        edt80MMYELLOWReflector = findViewById(R.id.edt80MMYELLOWReflector);
        txtActivityName = findViewById(R.id.txtActivityName);
        imgBack = findViewById(R.id.imgBack);


        txtRedTap20MM_id = findViewById(R.id.txtRedTap20MM_id);
        txtWhiteTap20MM_id = findViewById(R.id.txtWhiteTap20MM_id);
        txt50MMREDTap_id = findViewById(R.id.txt50MMREDTap_id);
        txt50MMWhite_id = findViewById(R.id.txt50MMWhite_id);
        txt50MMYellow_id = findViewById(R.id.txt50MMYellow_id);
        txtClass4_id = findViewById(R.id.txtClass4_id);
        txtClass3_id = findViewById(R.id.txtClass3_id);
        txt80MMREDReflector_id = findViewById(R.id.txt80MMREDReflector_id);
        txt80MMWHITEReflector_id = findViewById(R.id.txt80MMWHITEReflector_id);
        txt80MMYELLOWReflector_id = findViewById(R.id.txt80MMYELLOWReflector_id);

        imgHome  = findViewById(R.id.imgHome);
        imgHome.setVisibility(View.VISIBLE);

    }


    private void getData()
    {

        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            registration = bundle.getString("registration");
            registrationYear = bundle.getString("registrationYear");
            engineNo = bundle.getString("engineNo");
           // vehicleModel = bundle.getString("vehicleModel");
           // vechicleMaker = bundle.getString("vechicleMaker");
          //  ownerDetails = bundle.getString("ownerDetails");
            //address = bundle.getString("address");

            personName = bundle.getString("personName");
            personContact = bundle.getString("personContact");


            ownerName = bundle.getString("ownerName");
            ownerContact = bundle.getString("ownerContact");
            address = bundle.getString("address");
            gstNo = bundle.getString("gstNo");

            vechicleMaker = bundle.getString("vechicleMaker");
            vehicleModel = bundle.getString("vehicleModel");

           // distributorName = bundle.getString("distributorName");
            //mfgAddress = bundle.getString("mfgAddress");
           // distributorAddress = bundle.getString("distributorAddress");
            chesis = bundle.getString("chesis");
           // gstNo = bundle.getString("gstNo");

            userId = bundle.getString("userId");
            bussinessId = bundle.getString("bussinessId");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txtNext:
                validation();
                break;

            case R.id.imgBack:
                finish();
                break;

            case R.id.imgHome:
                Intent intent = new Intent(TapesActivity.this, DashBoardActivity.class);
                startActivity(intent);
                break;

                default:
                break;
        }
    }

    private void validation() {
        String red20_qty = edt20MMRedTap.getText().toString();
        String white20_qty  = edt20MMWhiteTap.getText().toString();
        String red50_qty  = edt50MMFitmentREDTap.getText().toString();
        String white50_qty  = edt50MMWhiteFitment.getText().toString();
        String yellow50_qty  = edt50MMYellowFitment.getText().toString();
       // String Case3_qty  = edtRearMarkingCase3.getText().toString();
      //  String Case4_qty  = edtRearMarkingClass4.getText().toString();
        String red80_qty  = edt80MMREDReflector.getText().toString();
        String white80_qty  = edt80MMWHITEReflector.getText().toString();
        String yellow80_qty  = edt80MMYELLOWReflector.getText().toString();

        if (yellow80_qty.isEmpty())
        {
            edt80MMYELLOWReflector.setError(getString(R.string.enter_reflector));
            edt80MMYELLOWReflector.requestFocus();
            return;
        }

        if (white80_qty.isEmpty())
        {
            edt80MMWHITEReflector.setError(getString(R.string.enter_reflector));
            edt80MMWHITEReflector.requestFocus();
            return;
        }


       /* if (Case3_qty.isEmpty())
        {
            edtRearMarkingCase3.setError(getString(R.string.enter_case));
            edtRearMarkingCase3.requestFocus();
            return;
        }*/

       /* if (Case4_qty.isEmpty())
        {
            edtRearMarkingClass4.setError(getString(R.string.enter_case));
            edtRearMarkingClass4.requestFocus();
            return;
        }*/

        if (red80_qty.isEmpty())
        {
            edt80MMREDReflector.setError(getString(R.string.enter_reflector));
            edt80MMREDReflector.requestFocus();
            return;
        }

        if (red20_qty.isEmpty())
        {
            edt20MMRedTap.setError(getString(R.string.enter_tap_length));
            edt20MMRedTap.requestFocus();
            return;
        }

        if (white20_qty.isEmpty())
        {
            edt20MMWhiteTap.setError(getString(R.string.enter_tap_length));
            edt20MMWhiteTap.requestFocus();
            return;
        }

        if (red50_qty.isEmpty())
        {
            edt50MMFitmentREDTap.setError(getString(R.string.enter_tap_length));
            edt50MMFitmentREDTap.requestFocus();
            return;
        }

        if (white50_qty.isEmpty())
        {
            edt50MMWhiteFitment.setError(getString(R.string.enter_tap_length));
            edt50MMWhiteFitment.requestFocus();
            return;
        }


        if (yellow50_qty.isEmpty())
        {
            edt50MMYellowFitment.setError(getString(R.string.enter_tap_length));
            edt50MMYellowFitment.requestFocus();
            return;
        }

        else {
            Intent intent = new Intent(TapesActivity.this,CaptureImagesActivity.class);
            intent.putExtra("red20_qty",red20_qty);
            intent.putExtra("white20_qty",white20_qty);
            intent.putExtra("red50_qty",red50_qty);
            intent.putExtra("white50_qty",white50_qty);
            intent.putExtra("yellow50_qty",yellow50_qty);
           // intent.putExtra("Case3_qty",Case3_qty);
           // intent.putExtra("Case4_qty",Case4_qty);
            intent.putExtra("red80_qty",red80_qty);
            intent.putExtra("white80_qty",white80_qty);
            intent.putExtra("yellow80_qty",yellow80_qty);


            intent.putExtra("registration", registration);
            intent.putExtra("chesis", chesis);
            intent.putExtra("registrationYear", registrationYear);
            intent.putExtra("engineNo", engineNo);

            intent.putExtra("vehicleModel", vehicleModel);
            intent.putExtra("vechicleMaker", vechicleMaker);

            intent.putExtra("personName", personName);
            intent.putExtra("personContact", personContact);

            intent.putExtra("ownerName", ownerName);
            intent.putExtra("gstNo", gstNo);
            intent.putExtra("ownerContact", ownerContact);
            intent.putExtra("address", address);


           // intent.putExtra("ownerDetails", ownerDetails);
           // intent.putExtra("address", address);
           // intent.putExtra("manufacturerName", manufacturerName);
           // intent.putExtra("contact", contact);
           // intent.putExtra("distributorName", distributorName);
           // intent.putExtra("mfgAddress", mfgAddress);
           // intent.putExtra("distributorAddress", distributorAddress);
            //intent.putExtra("gstNo", gstNo);

            intent.putExtra("userId", userId);
            intent.putExtra("bussinessId", bussinessId);



            String red_20mm_id ="2";
            String white_20mm_id ="5";

            String red_50mm_id ="4";
            String white_50mm_id ="6";
            String yellow_50mm_id ="9";

            //String txtClass4_id ="7";
           // String txtClass3_id ="8";

            String red_80mm_ref_id ="10";
            String white_80mm_ref_id ="11";
            String yellow_80mm_ref_id ="12";


            intent.putExtra("red_20mm_id", red_20mm_id);
            intent.putExtra("white_20mm_id", white_20mm_id);
            intent.putExtra("red_50mm_id", red_50mm_id);
            intent.putExtra("white_50mm_id", white_50mm_id);
            intent.putExtra("yellow_50mm_id", yellow_50mm_id);

           // intent.putExtra("txtClass4_id", txtClass4_id);
           // intent.putExtra("txtClass3_id", txtClass3_id);

            intent.putExtra("red_80mm_ref_id", red_80mm_ref_id);
            intent.putExtra("white_80mm_ref_id", white_80mm_ref_id);
            intent.putExtra("yellow_80mm_ref_id", yellow_80mm_ref_id);


            startActivity(intent);
        }

    }
}