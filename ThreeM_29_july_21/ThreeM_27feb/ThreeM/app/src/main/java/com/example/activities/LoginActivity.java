package com.example.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.fragments.UserFragment;
import com.example.model.LoginResponse;
import com.example.model.UserDataResponse;
import com.example.remote.RetrofitClient;
import com.example.threem.R;
import com.wang.avi.AVLoadingIndicatorView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity  implements View.OnClickListener {

    private EditText edtUserName,edtPassword;
    private TextView txtLogin;
    private AVLoadingIndicatorView progressBar;
    private ImageView imgBack;
    private TextView txtActivityName;
    private RelativeLayout rlMain;
    private ImageView imgHome;

    String name,bussinessName,fullName,country,landmark,regDate,email;

    String bussinessId =null;
    String userId=null;

    String username,password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
       /* getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.activity_login);
        //getSupportActionBar().hide();

        init();
        clickEvents();

        txtActivityName.setText("Login");
        imgHome.setVisibility(View.GONE);
    }

    private void clickEvents() {
        txtLogin.setOnClickListener(this);
        imgBack.setVisibility(View.GONE);
    }

    private void init() {
        edtUserName = findViewById(R.id.edtUserName);
        edtPassword = findViewById(R.id.edtPassword);
        txtLogin = findViewById(R.id.txtLogin);
        progressBar  = findViewById(R.id.progressBar);
        imgBack = findViewById(R.id.imgBack);
        txtActivityName = findViewById(R.id.txtActivityName);
        rlMain = findViewById(R.id.rlMain);
        imgHome = findViewById(R.id.imgHome);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txtLogin:
                validation();
                break;
        }
    }

    private void validation() {
         username = (edtUserName.getText().toString().trim());
         password = (edtPassword.getText().toString().trim());

        if (username.length() == 0) {
            edtUserName.setError(getString(R.string.username_error));
            edtUserName.requestFocus();
            return;
        }
        if (password.length() == 0) {
            edtPassword.setError(getString(R.string.password_error));
            edtPassword.requestFocus();
            return;
        }

        loginApi();

    }

    private void loginApi() {
        progressBar.show();

        Call<LoginResponse> call = RetrofitClient
                .getInstance()
                .getApi()
                .login(username,password);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {



                UserDataResponse dataResponse = response.body().getData();

                String user_name = dataResponse.getUsername();

                if (user_name!=null) {
                    bussinessId = String.valueOf(dataResponse.getBusinessId());
                    userId = String.valueOf(dataResponse.getUserId());

                    email = dataResponse.getUserEmail();
                    name = dataResponse.getUsername();
                    bussinessName = dataResponse.getBusinessName();
                    fullName = dataResponse.getFullname();
                    country = dataResponse.getCountry();
                    landmark = dataResponse.getLandmark();
                    regDate = dataResponse.getRegistredDate();

                }else
                {
                    progressBar.hide();
                    Toast.makeText(LoginActivity.this, "Wrong credential", Toast.LENGTH_SHORT).show();
                }

                String status = response.body().getStatus();
                if (status.equals("200"))
                {
                    Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
                    intent.putExtra("name",name);
                    intent.putExtra("bussinessName",bussinessName);
                    intent.putExtra("fullName",fullName);
                    intent.putExtra("country",country);
                    intent.putExtra("landmark",landmark);
                    intent.putExtra("regDate",regDate);
                    intent.putExtra("email",email);
                    intent.putExtra("bussinessId",bussinessId);
                    intent.putExtra("userId",userId);

                    startActivity(intent);
                    progressBar.hide();
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "Wrong credentials", Toast.LENGTH_SHORT).show();
                    progressBar.hide();

                }

                edtUserName.setText("");
                edtPassword.setText("");

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(LoginActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.hide();
            }
        });

    }

}