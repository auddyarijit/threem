package com.example.activities;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import com.example.threem.R;
import com.google.zxing.WriterException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class CertificateActivity extends AppCompatActivity {

//    private  TextView txtRegistrationNo;
//    private  TextView txtChesisNo;
//    private  TextView txtVechicleMake;
//    private  TextView txtRegistrationYear;
//    private  TextView txtEngineNo;
//    private  TextView txtVechicleModel;
//    private  TextView txtContactNo;
//    private  TextView txtAddress;
//    private  TextView txtManufacturerName;
//    private  TextView txtDistributorName;
//    private  TextView txtMfgAddress;
//    private  TextView txtDistributorAddress;
//
//    private  TextView txt20MMREDFitment;
//    private  TextView txt20MMRED;
//    private  TextView txt20MMWHITEFitment;
//    private  TextView txt20MM;
//    private  TextView txt50MMREDFitment;
//    private  TextView txt50MMFitment;
//    private  TextView txt50MMYELLOWFitment;
//    private  TextView txt50MMRED;
//    private  TextView txt50MM;
//    private  TextView txt80MMRED;
//    private  TextView txt80MMWHITE;
//
    private ImageView imgFront;
    private ImageView imgBack;
    private ImageView imgsideOne;
    private ImageView imgSideTwo;
//
//    String registration,registrationYear,engineNo,vehicleModel;
//    String vechicleMaker;
//    String chesis,gstNo,personName,personContact;
//    String ownerName,ownerContact,address;
//
//
//    String white_20mm_id;
//    String red_50mm_id ,white_50mm_id,yellow_50mm_id;
//    String txtClass4_id ,txtClass3_id;
//    String red_80mm_ref_id,white_80mm_ref_id,yellow_80mm_ref_id;
//
//    String red20_qty,white20_qty,red50_qty,white50_qty,white80_qty;
//    String  yellow50_qty,red80_qty,yellow80_qty;
//
//
    String img_front,img_back,img_side1,img_side2;
    Bitmap  bitmap,bitmapBack,bitmapSideOne,bitmapSideTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);
       // getSupportActionBar().hide();

//        txtRegistrationNo = findViewById(R.id.txtRegistrationNo);
//        txtChesisNo = findViewById(R.id.txtChesisNo);
//        txtVechicleMake = findViewById(R.id.txtVechicleMake);
//        txtRegistrationYear = findViewById(R.id.txtRegistrationYear);
//        txtEngineNo = findViewById(R.id.txtEngineNo);
//
//        txtVechicleModel = findViewById(R.id.txtVechicleModel);
//        txtContactNo = findViewById(R.id.txtContactNo);
//        txtAddress = findViewById(R.id.txtAddress);
//        txtManufacturerName = findViewById(R.id.txtManufacturerName);
//        txtDistributorName = findViewById(R.id.txtDistributorName);
//        txtMfgAddress = findViewById(R.id.txtMfgAddress);
//
//        txtDistributorAddress = findViewById(R.id.txtDistributorAddress);
//        txt20MMREDFitment = findViewById(R.id.txt20MMREDFitment);
//        txt20MMRED = findViewById(R.id.txt20MMRED);
//        txt20MMWHITEFitment = findViewById(R.id.txt20MMWHITEFitment);
//        txt20MM = findViewById(R.id.txt20MM);
//        txt50MM = findViewById(R.id.txt50MM);
//
//        txt50MMREDFitment = findViewById(R.id.txt50MMREDFitment);
//        txt50MMFitment = findViewById(R.id.txt50MMFitment);
//        txt50MMYELLOWFitment = findViewById(R.id.txt50MMYELLOWFitment);
//        txt50MMRED = findViewById(R.id.txt50MMRED);
//        txt80MMRED = findViewById(R.id.txt80MMRED);
//        txt80MMWHITE = findViewById(R.id.txt80MMWHITE);
//
        imgFront = findViewById(R.id.imgFront);
        imgBack = findViewById(R.id.imgBack);
        imgsideOne = findViewById(R.id.imgsideOne);
        imgSideTwo = findViewById(R.id.imgSideTwo);
//
//
//
        getData();
//        setData();

    }

//    private void setData() {
//        txtRegistrationNo.setText(registration);
//        txtChesisNo.setText(chesis);
//        txtRegistrationYear.setText(registrationYear);
//        txtEngineNo.setText(engineNo);
//
//        txtVechicleModel.setText(vehicleModel);
//        txtVechicleMake.setText(vechicleMaker);
//        txtDistributorName.setText(personName);
//        txtManufacturerName.setText(ownerName);
//        txtContactNo.setText(personContact);
//        txt20MMRED.setText(red20_qty);
//
//        txt20MMRED.setText(red20_qty);
//        txt20MM.setText(white20_qty);
//        txt50MMRED.setText(red50_qty);
//        txt50MM.setText(white50_qty);
//        txt80MMRED.setText(red80_qty);
//        txt80MMWHITE.setText(white80_qty);
//    }

    private void getData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle!=null)
        {
//            registration = bundle.getString("registration");
//            chesis = bundle.getString("chesis");
//            registrationYear = bundle.getString("registrationYear");
//            engineNo = bundle.getString("engineNo");
//            vehicleModel = bundle.getString("vehicleModel");
//            vechicleMaker = bundle.getString("vechicleMaker");
//            personName = bundle.getString("personName");
//            personContact = bundle.getString("personContact");
//            ownerName = bundle.getString("ownerName");
//            ownerContact = bundle.getString("ownerContact");
//
//
//            red20_qty = bundle.getString("red20_qty");
//            white20_qty = bundle.getString("white20_qty");
//            red50_qty = bundle.getString("red50_qty");
//            white50_qty = bundle.getString("white50_qty");
//            yellow50_qty = bundle.getString("yellow50_qty");
//            white80_qty = bundle.getString("white80_qty");
//            red80_qty = bundle.getString("red80_qty");
//            yellow80_qty = bundle.getString("yellow80_qty");
//            red80_qty = bundle.getString("red80_qty");

//            img_front = bundle.getString("img_front");
//            img_back = bundle.getString("img_back");
//            img_side1 = bundle.getString("img_side1");
//            img_side2 = bundle.getString("img_side2");


            Intent intent = getIntent();

             bitmap = (Bitmap) intent.getParcelableExtra("bitmap");
             bitmapBack = (Bitmap) intent.getParcelableExtra("bitmapBack");
             bitmapSideOne = (Bitmap) intent.getParcelableExtra("bitmapSideOne");
             bitmapSideTwo = (Bitmap) intent.getParcelableExtra("bitmapSideTwo");
             imgFront.setImageBitmap(bitmap);
             imgBack.setImageBitmap(bitmapBack);
             imgsideOne.setImageBitmap(bitmapSideOne);
             imgSideTwo.setImageBitmap(bitmapSideTwo);
             Toast.makeText(this, "", Toast.LENGTH_SHORT).show();

        }

    }


}