package com.example.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adapter.InvoiceHistoryAdapter;
import com.example.model.InvoiceHistoryResponse;
import com.example.model.InvoiceHistoryResponseData;
import com.example.remote.RetrofitClient;
import com.example.threem.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imgBack;
    private TextView txtActivityName;
    private RecyclerView recycleView;
    private AVLoadingIndicatorView progressBar;
    private RelativeLayout rlMain,rlHistory;

    String userId =null;

    InvoiceHistoryAdapter adapter;

    List<InvoiceHistoryResponseData> historyDataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_voice_history);

        init();
        clickEvents();
        getData();
        getApi();

        txtActivityName.setText("History");
    }

    private void clickEvents() {
        imgBack.setOnClickListener(this);
    }


    private void getData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle!=null)
        {
            userId = bundle.getString("userId");
            Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
        }
    }


    private void init() {
        imgBack = findViewById(R.id.imgBack);
        txtActivityName = findViewById(R.id.txtActivityName);
        recycleView = findViewById(R.id.recycleView);
        progressBar = findViewById(R.id.progressBar);
        rlMain = findViewById(R.id.rlMain);
        rlHistory = findViewById(R.id.rlHistory);
    }



    private void getApi() {
        progressBar.show();

        Call<InvoiceHistoryResponse> call = RetrofitClient
                .getInstance()
                .getApi()
                .invoiceResponse(userId);

        call.enqueue(new Callback<InvoiceHistoryResponse>() {
            @Override
            public void onResponse(Call<InvoiceHistoryResponse> call, Response<InvoiceHistoryResponse> response) {
                String status = response.body().getStatus();

                if (status.equals("200")){
                   historyDataList = response.body().getData();
                } if (historyDataList!=null)
                {
                    setAdapter();
                }else{
                    rlHistory.setVisibility(View.VISIBLE);
                }
                progressBar.hide();
            }

            @Override
            public void onFailure(Call<InvoiceHistoryResponse> call, Throwable t) {
                progressBar.hide();
                Toast.makeText(HistoryActivity.this, "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setAdapter() {
        adapter = new InvoiceHistoryAdapter(this, historyDataList);
        RecyclerView.LayoutManager histrory = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recycleView.setLayoutManager(histrory);
        recycleView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.imgBack:
                finish();
                break;
        }
    }
}