package com.example.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.threem.R;
import com.google.android.material.navigation.NavigationView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.rom4ek.arcnavigationview.ArcNavigationView;

import java.util.HashMap;

import static android.Manifest.permission.CALL_PHONE;

public class DashBoardActivity extends AppCompatActivity implements View.OnClickListener,NavigationView.OnNavigationItemSelectedListener {

    private RelativeLayout rl;
    private ArcNavigationView nav_view;

    private ActionBarDrawerToggle toggle;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private TextView txtName;
    static final float END_SCALE = 0.7f;

    String name,bussinessName,fullName,country,landmark,regDate,email;
    String bussinessId,userId;

    private CardView card_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        init();
        clickEvents();
        getData();
        setData();

        setSupportActionBar(toolbar);

        toggle=new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.nav_open,R.string.nav_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));


        //set data on navigation header
        View header = nav_view.getHeaderView(0);
        txtName = header.findViewById(R.id.txtName);
        txtName.setText(fullName);

        naviagtionDrawer();

    }



    private void setData() {
       // txtName.setText(fullName);
    }

    private void clickEvents() {
        card_view.setOnClickListener(this);
    }


    private void getData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle!=null)
        {
            name = bundle.getString("name");
            bussinessName = bundle.getString("bussinessName");
            fullName = bundle.getString("fullName");
            country = bundle.getString("country");
            landmark = bundle.getString("landmark");
            regDate = bundle.getString("regDate");
            bussinessId = bundle.getString("bussinessId");
            userId = bundle.getString("userId");
            email= bundle.getString("email");
        }
    }



    private void init() {
        card_view = findViewById(R.id.card_view);
        rl = findViewById(R.id.rl);
        toolbar = findViewById(R.id.toolbar);
        nav_view=(ArcNavigationView)findViewById(R.id.nav_view);
        drawerLayout=(DrawerLayout)findViewById(R.id.drawerLayout);
       // txtName=findViewById(R.id.txtName);
    }

    //Navigation Drawer Functions
    private void naviagtionDrawer() {
        //Naviagtion Drawer
        nav_view.bringToFront();
        nav_view.setNavigationItemSelectedListener(this);

      //  nav_view.setCheckedItem(R.id.nav_home);

        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerLayout.isDrawerVisible(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                else drawerLayout.openDrawer(GravityCompat.START);
            }
        });

        animateNavigationDrawer();

    }

    private void animateNavigationDrawer() {

        //Add any color or remove it to use the default one!
        //To make it transparent use Color.Transparent in side setScrimColor();
        drawerLayout.setScrimColor(Color.TRANSPARENT);
        drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

                // Scale the View based on current slide offset
                final float diffScaledOffset = slideOffset * (1 - END_SCALE);
                final float offsetScale = 1 - diffScaledOffset;
                rl.setScaleX(offsetScale);
                rl.setScaleY(offsetScale);

                // Translate the View, accounting for the scaled width
                final float xOffset = drawerView.getWidth() * slideOffset;
                final float xOffsetDiff = rl.getWidth() * diffScaledOffset / 2;
                final float xTranslation = xOffset - xOffsetDiff;
                rl.setTranslationX(xTranslation);
            }
        });

    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerVisible(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else
            super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.card_view:

                Intent intent = new Intent(DashBoardActivity.this,VechicalDetailsActivity.class);
                intent.putExtra("bussinessId",bussinessId);
                intent.putExtra("userId",userId);
                startActivity(intent);
                break;
        }
    }



    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {

            case R.id.nav_home :
                startActivity(new Intent(DashBoardActivity.this, DashBoardActivity.class));
                drawerLayout.closeDrawer(GravityCompat.START);
                break;


            case R.id.nav_profile :
                sendData();
                drawerLayout.closeDrawer(GravityCompat.START);
                break;

            case R.id.nav_history :

                Intent intent = new Intent(DashBoardActivity.this, HistoryActivity.class);
                intent.putExtra("userId",userId);
                startActivity(intent);
                //startActivity(new Intent(DashBoardActivity.this, InVoiceHistoryActivity.class));
                drawerLayout.closeDrawer(GravityCompat.START);
                break;

            case R.id.nav_support:
                SupportDailog();
                break;


            case R.id.nav_about_us:
                startActivity(new Intent(DashBoardActivity.this, AboutUsActivity.class));
                break;


            case R.id.nav_share:
                share();
                break;
        }

        return true;
    }

    private void share() {
        Intent shareIntent =   new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT,"Insert Subject here");
        String app_url = " http://techsumitsolutions.com/";
        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,app_url);
        startActivity(Intent.createChooser(shareIntent, "Share via"));
    }

    private void SupportDailog() {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.support_dailog, viewGroup, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setView(dialogView);

        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView txtCancel = dialogView.findViewById(R.id.txtCancel);
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        TextView txtCallNo = dialogView.findViewById(R.id.txtCallNo);
        txtCallNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPermission();
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    private void callPermission() {

            Dexter.withActivity(this)
                    .withPermission(CALL_PHONE)
                    .withListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted(PermissionGrantedResponse response) {
                            String number="9589206597";
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:"+number));
                            startActivity(callIntent);
                        }

                        @Override
                        public void onPermissionDenied(PermissionDeniedResponse response) {
                            // check for permanent denial of permission
                            if (response.isPermanentlyDenied()) {
                                Toast.makeText(DashBoardActivity.this, "please give call permission", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).check();
    }


    private void sendData() {
        Intent intent = new Intent(DashBoardActivity.this, ProfileActivity.class);
        intent.putExtra("name",name);
        intent.putExtra("bussinessName",bussinessName);
        intent.putExtra("fullName",fullName);
        intent.putExtra("country",country);
        intent.putExtra("landmark",landmark);
        intent.putExtra("regDate",regDate);
        intent.putExtra("email",email);
        intent.putExtra("bussinessId",bussinessId);
        intent.putExtra("userId",userId);
        startActivity(intent);
    }
}