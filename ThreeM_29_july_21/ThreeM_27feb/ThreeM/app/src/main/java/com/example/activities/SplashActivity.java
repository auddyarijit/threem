package com.example.activities;

import android.content.Intent;
import android.os.Bundle;

import android.os.Handler;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.threem.R;

import pl.droidsonroids.gif.GifImageView;

public class SplashActivity extends AppCompatActivity {

    //private GifImageView gifImag;

    private ImageView imgTechSum;
    private static int SPLASH_TIME_OUT = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
       // getSupportActionBar().hide();

        imgTechSum = findViewById(R.id.imgTechSum);
      //  gifImag = findViewById(R.id.gifImag);

        Animation an2 = AnimationUtils.loadAnimation(this, R.anim.splash_anim);
        imgTechSum.startAnimation(an2);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
            }
        }, SPLASH_TIME_OUT);

    }
}