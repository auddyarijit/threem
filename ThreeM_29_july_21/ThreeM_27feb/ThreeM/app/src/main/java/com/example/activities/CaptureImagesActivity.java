package com.example.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.model.FormInvoiceResponse;
import com.example.remote.RetrofitClient;
import com.example.threem.R;
import com.wang.avi.AVLoadingIndicatorView;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CaptureImagesActivity extends AppCompatActivity implements View.OnClickListener {

    String red20_qty,white20_qty,red50_qty,white50_qty,white80_qty;
    String  yellow50_qty,red80_qty,yellow80_qty;

    private AppCompatButton btnSubmit;
    private ImageView imgBack;
    private TextView txtActivityName;
    private AVLoadingIndicatorView progressBar;
    private ImageView imgHome;
    private ImageButton btnFrontCancel;
    private ImageButton btnCancelBackSide;
    private ImageButton btnCancelSideOne;
    private ImageButton btnCancelSideTwo;

    String registration,registrationYear,engineNo,vehicleModel;
    String vechicleMaker;
    String chesis,gstNo,personName,personContact;
    String ownerName,ownerContact,address;


    Bitmap  bitmap,bitmapBack,bitmapSideOne,bitmapSideTwo;

  //  private EditText edtTypeNumber,edtCop;
   // private EditText edtTestReport,edtEcMark,edtCertifiedBy;

    String userId =null;
    String bussinessId =null;

    String img_front,img_back,img_side1,img_side2;



    private ImageView imgFront,imgBackSide,imgSideOne,imgSideTwo;
    public  static final int RequestPermissionCode  = 1 ;


    ArrayList<String> imageList = new ArrayList<>();


    String red_20mm_id,white_20mm_id;
    String red_50mm_id ,white_50mm_id,yellow_50mm_id;
    String txtClass4_id ,txtClass3_id;
    String red_80mm_ref_id,white_80mm_ref_id,yellow_80mm_ref_id;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture_images);
       // getSupportActionBar().hide();
        EnableRuntimePermission();

        init();
        getData();
        clickEvent();

        txtActivityName.setText("Certificate Details");

    }

    private void clickEvent() {
        imgFront.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        imgSideOne.setOnClickListener(this);
        imgSideTwo.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        imgBackSide.setOnClickListener(this);
        imgHome.setOnClickListener(this);



        btnFrontCancel.setOnClickListener(this);
        btnCancelBackSide.setOnClickListener(this);
        btnCancelSideOne.setOnClickListener(this);
        btnCancelSideTwo.setOnClickListener(this);
    }


    private String convertToString(Bitmap bitmap)
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] imageBytes = byteArrayOutputStream.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }


    private void captureImage() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 1);
    }

    private void captureImageBack() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 2);
    }

    private void captureSideOne() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 3);
    }

    private void captureimgSideTwo() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 4);
    }




    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK && data!=null) {
              bitmap= (Bitmap) data.getExtras().get("data");
            imgFront.setImageBitmap(bitmap);
            String one =  convertToString(bitmap);
            img_front = one;


            if (img_front!=null)
            {
                btnFrontCancel.setVisibility(View.VISIBLE);
            }else{
                imgFront.setImageResource(R.drawable.camera);
            }

            //   imageList.add("'0_"+one+"'");
        }
        else if (requestCode == 2 && resultCode == RESULT_OK) {
             bitmapBack = (Bitmap) data.getExtras().get("data");
            imgBackSide.setImageBitmap(bitmapBack);
            String two =   convertToString(bitmapBack);
            img_back =two;

            if (img_back!=null)
            {
                btnCancelBackSide.setVisibility(View.VISIBLE);
            }

           // imageList.add("'1_"+two+"'");
            // imageList.add(String.valueOf(image));
        }

        else if (requestCode == 3 && resultCode == RESULT_OK) {
             bitmapSideOne = (Bitmap) data.getExtras().get("data");
            imgSideOne.setImageBitmap(bitmapSideOne);
            String three =   convertToString(bitmapSideOne);
            img_side1 =three;


            if (img_side1!=null)
            {
                btnCancelSideOne.setVisibility(View.VISIBLE);
            }
           // imageList.add("'2_"+three+"'");
            // imageList.add(String.valueOf(image));
        }

        else if (requestCode == 4 && resultCode == RESULT_OK) {
             bitmapSideTwo = (Bitmap) data.getExtras().get("data");
            imgSideTwo.setImageBitmap(bitmapSideTwo);
            String four =   convertToString(bitmapSideTwo);
            img_side2 = four;

            if (img_side2!=null)
            {
                btnCancelSideTwo.setVisibility(View.VISIBLE);
            }
           // imageList.add("'3_"+four+"'");
            //imageList.add(String.valueOf(image));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public void EnableRuntimePermission(){
        if (ActivityCompat.shouldShowRequestPermissionRationale(CaptureImagesActivity.this,
                Manifest.permission.CAMERA))
        {
            Toast.makeText(CaptureImagesActivity.this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(CaptureImagesActivity.this,new String[]{
                    Manifest.permission.CAMERA}, RequestPermissionCode);
        }

    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(CaptureImagesActivity.this,"Permission Granted", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(CaptureImagesActivity.this,"Permission Canceled", Toast.LENGTH_LONG).show();
                }

                break;
        }
    }


    private void getData()
    {
        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){

            registration = bundle.getString("registration");
            chesis = bundle.getString("chesis");
            registrationYear = bundle.getString("registrationYear");
            engineNo = bundle.getString("engineNo");
            vehicleModel = bundle.getString("vehicleModel");
            vechicleMaker = bundle.getString("vechicleMaker");
            //  ownerDetails = bundle.getString("ownerDetails");
            //address = bundle.getString("address");

            personName = bundle.getString("personName");
            personContact = bundle.getString("personContact");

            ownerName = bundle.getString("ownerName");
            ownerContact = bundle.getString("ownerContact");
            address = bundle.getString("address");
            gstNo = bundle.getString("gstNo");



           // ownerDetails = bundle.getString("ownerDetails");
           // address = bundle.getString("address");
          //  manufacturerName = bundle.getString("manufacturerName");
           // contact = bundle.getString("contact");
           // distributorName = bundle.getString("distributorName");
           // mfgAddress = bundle.getString("mfgAddress");
           // distributorAddress = bundle.getString("distributorAddress");


            personName = bundle.getString("personName");
            personContact = bundle.getString("personContact");

            ownerName = bundle.getString("ownerName");
            gstNo = bundle.getString("gstNo");
            ownerContact = bundle.getString("ownerContact");
            address = bundle.getString("address");


            red20_qty = bundle.getString("red20_qty");
            white20_qty = bundle.getString("white20_qty");

            red50_qty = bundle.getString("red50_qty");
            white50_qty = bundle.getString("white50_qty");
            yellow50_qty = bundle.getString("yellow50_qty");

           // Case3_qty = bundle.getString("Case3_qty");
           // Case4_qty = bundle.getString("Case4_qty");

            red80_qty = bundle.getString("red80_qty");
            yellow80_qty = bundle.getString("yellow80_qty");
            white80_qty = bundle.getString("white80_qty");


            userId = bundle.getString("userId");
            bussinessId = bundle.getString("bussinessId");


            red_20mm_id = bundle.getString("red_20mm_id");
            white_20mm_id = bundle.getString("white_20mm_id");

            red_50mm_id = bundle.getString("red_50mm_id");
            white_50mm_id = bundle.getString("white_50mm_id");
            yellow_50mm_id = bundle.getString("yellow_50mm_id");

           // txtClass4_id = bundle.getString("txtClass4_id");
          //  txtClass3_id = bundle.getString("txtClass3_id");

            red_80mm_ref_id = bundle.getString("red_80mm_ref_id");
            white_80mm_ref_id = bundle.getString("white_80mm_ref_id");
            yellow_80mm_ref_id = bundle.getString("yellow_80mm_ref_id");


        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnSubmit:
                imageValidation();
               // invoiceFormApi();
                break;

            case R.id.imgBack:
                finish();
                break;


            case R.id.imgFront:
                captureImage();
                break;

            case R.id.imgBackSide:
                captureImageBack();
                break;


            case R.id.imgSideOne:
                captureSideOne();
                break;

            case R.id.imgSideTwo:
                captureimgSideTwo();
                break;


            case R.id.imgHome:
                Intent intent = new Intent(CaptureImagesActivity.this, DashBoardActivity.class);
                startActivity(intent);
                break;


            case R.id.btnFrontCancel:
                if (img_front!=null)
                {
                    img_front = "";
                    imgFront.setImageResource(R.drawable.camera);
                    btnFrontCancel.setVisibility(View.GONE);
                }

                break;


            case R.id.btnCancelBackSide:
                if (img_back!=null)
                {
                    img_back = "";
                    imgBackSide.setImageResource(R.drawable.camera);
                    btnCancelBackSide.setVisibility(View.GONE);
                }

                break;


            case R.id.btnCancelSideOne:
                if (img_side1!=null)
                {
                    img_side1 = "";
                    imgSideOne.setImageResource(R.drawable.camera);
                    btnCancelSideOne.setVisibility(View.GONE);
                }
                break;


            case R.id.btnCancelSideTwo:
                if (img_side2!=null)
                {
                    img_side2 = "";
                    imgSideTwo.setImageResource(R.drawable.camera);
                    btnCancelSideTwo.setVisibility(View.GONE);
                }
                break;



            default:
                break;
        }
    }

    private void imageValidation() {
        if(img_front==null)
        {
            Toast.makeText(this, "Please Capture Front side ", Toast.LENGTH_SHORT).show();
        }
        else if (img_back==null){
            Toast.makeText(this, "Please Capture Back side", Toast.LENGTH_SHORT).show();
        }
        else if (img_side1==null)
        {
            Toast.makeText(this, "Please Capture Side image", Toast.LENGTH_SHORT).show();
        }
        else if (img_side2==null)
        {
            Toast.makeText(this, "Please Capture side  image", Toast.LENGTH_SHORT).show();
        }
         else {
            invoiceFormApi();
        }
    }

    private void init() {
       // edtTypeNumber = findViewById(R.id.edtTypeNumber);
       // edtCop = findViewById(R.id.edtCop);
       // edtTestReport = findViewById(R.id.edtTestReport);
       // edtEcMark = findViewById(R.id.edtEcMark);
       // edtCertifiedBy = findViewById(R.id.edtCertifiedBy);
        btnSubmit = findViewById(R.id.btnSubmit);
        imgBack = findViewById(R.id.imgBack);
        txtActivityName = findViewById(R.id.txtActivityName);

        imgFront = findViewById(R.id.imgFront);
        imgBackSide = findViewById(R.id.imgBackSide);
        imgSideOne = findViewById(R.id.imgSideOne);
        imgSideTwo = findViewById(R.id.imgSideTwo);
        btnSubmit = findViewById(R.id.btnSubmit);


        btnFrontCancel = findViewById(R.id.btnFrontCancel);
        btnCancelBackSide = findViewById(R.id.btnCancelBackSide);
        btnCancelSideOne = findViewById(R.id.btnCancelSideOne);
        btnCancelSideTwo = findViewById(R.id.btnCancelSideTwo);

        progressBar = findViewById(R.id.progressBar);
        imgHome  = findViewById(R.id.imgHome);
        imgHome.setVisibility(View.VISIBLE);


    }





    private void invoiceFormApi() {
        progressBar.show();

        Call<FormInvoiceResponse> call = RetrofitClient
            .getInstance()
            .getApi()
            .invoiceForm(registration,chesis,registrationYear,engineNo
                        ,vehicleModel,vechicleMaker,personName,personContact,ownerName,gstNo,
                    ownerContact,address,img_front,img_back,img_side1,img_side2,
                    bussinessId,userId,red20_qty,white20_qty
                    ,red50_qty,white50_qty,yellow50_qty
                    ,white80_qty,red80_qty,yellow80_qty,
                    red_20mm_id,white_20mm_id,red_50mm_id,white_50mm_id,
                    yellow_50mm_id,red_80mm_ref_id,white_80mm_ref_id,yellow_80mm_ref_id);

        call.enqueue(new Callback<FormInvoiceResponse>() {
            @Override
            public void onResponse(Call<FormInvoiceResponse> call, Response<FormInvoiceResponse> response) {


                String status = response.body().getStatus();

                if (status.equals("200")){


                    Intent intent = new Intent(CaptureImagesActivity.this,CertificateActivity.class);
//                    intent.putExtra("img_front",img_front);
//                    intent.putExtra("img_back",img_back);
//                    intent.putExtra("img_side1",img_side1);
//                    intent.putExtra("img_side2",img_side2);
//                    intent.putExtra("registration",registration);
//                    intent.putExtra("chesis",chesis);
//                    intent.putExtra("registrationYear",registrationYear);
//                    intent.putExtra("engineNo",engineNo);
//                    intent.putExtra("vehicleModel",vehicleModel);
//                    intent.putExtra("vechicleMaker",vechicleMaker);
//                    intent.putExtra("personName",personName);
//                    intent.putExtra("personContact",personContact);
//                    intent.putExtra("ownerName",ownerName);
//                    intent.putExtra("ownerContact",ownerContact);
//                    intent.putExtra("address",address);
//                    intent.putExtra("red20_qty",red20_qty);
//                    intent.putExtra("white20_qty",white20_qty);
//                    intent.putExtra("red50_qty",red50_qty);
//                    intent.putExtra("white50_qty",white50_qty);
//                    intent.putExtra("yellow50_qty",yellow50_qty);
//                    intent.putExtra("white80_qty",white80_qty);
//                    intent.putExtra("red80_qty",red80_qty);
//                    intent.putExtra("yellow80_qty",yellow80_qty);
//                    intent.putExtra("red80_qty",red80_qty);
//                    intent.putExtra("red_20mm_id",red_20mm_id);
//                    intent.putExtra("white_20mm_id",white_20mm_id);
//                    intent.putExtra("red_50mm_id",red_50mm_id);
//                    intent.putExtra("white_50mm_id",white_50mm_id);
//                    intent.putExtra("yellow_50mm_id",yellow_50mm_id);
//                    intent.putExtra("red_80mm_ref_id",red_80mm_ref_id);
//                    intent.putExtra("white_80mm_ref_id",white_80mm_ref_id);
//                    intent.putExtra("yellow_80mm_ref_id",yellow_80mm_ref_id);
                    intent.putExtra("bitmap",bitmap);
                    intent.putExtra("bitmapBack",bitmapBack);
                    intent.putExtra("bitmapSideOne",bitmapSideOne);
                    intent.putExtra("bitmapSideTwo",bitmapSideTwo);
                    startActivity(intent);


                    //startActivity(new Intent(CaptureImagesActivity.this, CertificateActivity.class));
                    progressBar.hide();
                }else {
                    progressBar.hide();
                    Toast.makeText(CaptureImagesActivity.this, "Some went wrong", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<FormInvoiceResponse> call, Throwable t) {
                progressBar.hide();
                Toast.makeText(CaptureImagesActivity.this, ""+t, Toast.LENGTH_SHORT).show();
            }
        });
    }





    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


}
